package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import utilities.Configuration;

import java.util.concurrent.TimeUnit;

public class AmazonHomePage {
    WebDriver driver;

    public AmazonHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static Logger logger = LoggerFactory.getLogger(AmazonHomePage.class);

    @FindBy(id = "twotabsearchtextbox")
    public WebElement searchBox;
    @FindBy(xpath = "(//input[@type='submit'])[1]")
    public WebElement searchSubmit;
    @FindBy(xpath = "//li[@class=\"a-normal\"]//a[contains(@href,'2')]")

    public WebElement secondPage;

    public void navigatesToAmazon(String string) {
        string = Configuration.getProperty("url");
        driver.get(string);
        logger.info("opening amazon home page");
    }


    public void clickSearchBox() {

        searchBox.click();
    }

    public void submitSearch(String string) {

        searchBox.click();
        searchBox.sendKeys(string);
        searchBox.submit();
        logger.info("displaying search result list");

    }

    public void clickSecondPage() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        secondPage.click();
        logger.info("navigates to the 2nd page");
    }


    @FindBy(xpath = "(//span[@class=\"a-size-medium a-color-base a-text-normal\"])[3]")
    public WebElement thirdItem;

    public void selectThirdItem() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        thirdItem.click();
        logger.info("selecting 3rd item");
    }

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    public WebElement addToCartButton;

    public void addToCart() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        addToCartButton.click();
        logger.info("added 3rd item to the cart");
    }

    @FindBy(xpath = "//i[@class=\"a-icon a-icon-close\"]")
    public WebElement closePopUp;

    @FindBy(xpath = "//button[@id=\"attachSiNoCoverage-announce\"]")
    public WebElement closePopUp2;

    public void closePopup() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        try {

            closePopUp.click();
            logger.info("popup closed");
        } catch (Exception e) {
            logger.info("no popup");
        }
        try {
            closePopUp2.click();
            logger.info("popup closed");
        } catch (Exception e) {
            logger.info("no popup2");
        }
    }


    @FindBy(xpath = "(//span[@class=\"a-size-medium a-align-center huc-subtotal\"])[1]")
    public WebElement itemInCart;



    public void validateInTheCart() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        String actualResult = itemInCart.getText();
        String expectedResult = "1 item";
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        Assert.assertTrue(actualResult.contains(expectedResult));
    }
}
