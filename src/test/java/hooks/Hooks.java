package hooks;

import io.cucumber.java.Before;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import testDriver.TestDriver;

public class Hooks {

    public static WebDriver driver;

    @Before
    public void setup() {

        String log4jPath = System.getProperty("user.dir") + "\\log4j.properties";
        PropertyConfigurator.configure(log4jPath);

        driver = TestDriver.getDriver();
        BasicConfigurator.configure();
    }


}
