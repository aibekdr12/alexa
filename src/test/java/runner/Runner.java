package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "@alexaSearch",
        features = "src/test/resources/features/",
        glue = "",
        plugin ={"pretty"},
        dryRun = false,
        strict = true
)
public class Runner {
}
