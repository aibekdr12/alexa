package steps;

import hooks.Hooks;
import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.AmazonHomePage;

public class AlexaSearch {
    private WebDriver driver= Hooks.driver;
    private static Logger logger = LoggerFactory.getLogger(AlexaSearch.class);
    AmazonHomePage amazonHomePage = new AmazonHomePage(driver);

    @Given("the user navigates to {string}")
    public void the_user_navigates_to(String string) {
        amazonHomePage.navigatesToAmazon(string);
    }

    @Given("Searches for {string}")
    public void searches_for(String string) {
        amazonHomePage.clickSearchBox();
        amazonHomePage.submitSearch(string);
    }

    @Given("navigates to the second page")
    public void navigates_to_the_second_page() throws InterruptedException {
        amazonHomePage.clickSecondPage();
    }

    @Given("selects the third item")
    public void selects_the_third_item() throws InterruptedException {
        amazonHomePage.selectThirdItem();
    }

    @Then("the user should be able to add it to the cart")
    public void the_user_should_be_able_to_add_it_to_the_cart() throws InterruptedException {
        amazonHomePage.addToCart();
        amazonHomePage.closePopup();
        amazonHomePage.validateInTheCart();
        logger.info("validated the item is in the cart");
    }
}
